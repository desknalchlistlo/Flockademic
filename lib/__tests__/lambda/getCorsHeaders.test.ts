import { getCorsHeaders } from '../../lambda/getCorsHeaders';

it('should add an Access-Control-Allow-Origin header for top-level flockademic.com Origins', () => {
  expect(getCorsHeaders({ Origin: 'https://flockademic.com' }))
  .toEqual({ 'Access-Control-Allow-Origin': 'https://flockademic.com' });
});

it('should add an Access-Control-Allow-Origin header for *.flockademic.com Origins', () => {
  expect(getCorsHeaders({ Origin: 'https://arbitrary.flockademic.com' }))
  .toEqual({ 'Access-Control-Allow-Origin': 'https://arbitrary.flockademic.com' });
});

it('should add an Access-Control-Allow-Origin header regardless of the capitalisation of the Origin header', () => {
  expect(getCorsHeaders({ orIgiN: 'https://arbitrary.flockademic.com' }))
  .toEqual({ 'Access-Control-Allow-Origin': 'https://arbitrary.flockademic.com' });
});

it('should not add an Access-Control-Allow-Origin header for non-Flockademic Origins', () => {
  expect(getCorsHeaders({ Origin: 'https://pretending.to.be.flockademic.com.example.com' }))
  .toEqual({});
});

it('should not add an Access-Control-Allow-Origin header when no Origin is specified', () => {
  expect(getCorsHeaders({}))
  .toEqual({});
});

it('should not add an Access-Control-Allow-Origin header when no headers are provided', () => {
  expect(getCorsHeaders())
  .toEqual({});

  expect(getCorsHeaders(null))
  .toEqual({});
});
