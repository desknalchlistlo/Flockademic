declare module 'whatwg-url' {
    // Node's type definitions are already up-to-date with Node 8, so we can simply re-export those
    // (Since the only reason we're using this module is to prepare for Node 8 anyway.)
    import * as nativeUrl from 'url';

    export import URL = nativeUrl.URL;
}
