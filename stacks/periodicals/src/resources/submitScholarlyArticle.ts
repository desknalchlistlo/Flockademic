import {
  PostSubmitScholarlyArticleRequest, PostSubmitScholarlyArticleResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { isArticleAuthor } from '../../../../lib/utils/periodicals';
import {
  scholarlyArticleDescriptionValidators,
  scholarlyArticleNameValidators,
} from '../../../../lib/validation/scholarlyArticle';
import { isValid } from '../../../../lib/validation/validators';
import { fetchScholarlyArticle } from '../services/scholarlyArticle';
import { submitScholarlyArticle as service } from '../services/submitScholarlyArticle';

export async function submitScholarlyArticle(
  context: Request<PostSubmitScholarlyArticleRequest> & DbContext & SessionContext,
): Promise<PostSubmitScholarlyArticleResponse> {
  if (!context.body) {
    throw new Error('Please specify an article and a journal to submit it to.');
  }

  if (!context.body.result || !context.body.result.isPartOf || !context.body.result.isPartOf.identifier) {
    throw new Error('Please specify a journal to submit this article to.');
  }
  const periodicalIdentifier = context.body.result.isPartOf.identifier;

  if (
    !context.body ||
    !context.body.targetCollection ||
    !context.body.targetCollection.identifier
  ) {
    throw new Error('Please specify an article to submit.');
  }
  const articleIdentifier = context.body.targetCollection.identifier;

  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }
  const session = context.session;

  if (!session.account) {
    throw new Error('You can only submit an article if you have created an account.');
  }

  const storedArticle = await fetchScholarlyArticle(context.database, articleIdentifier, context.session);

  if (storedArticle === null) {
    throw new Error(`Could not find an article with ID ${articleIdentifier}.`);
  }

  const articleAuthor = (storedArticle.author) ? storedArticle.author : undefined;
  if (
      storedArticle.creatorSessionId !== session.identifier
      && (!articleAuthor || !isArticleAuthor({ author: articleAuthor }, context.session))
   ) {
    throw new Error('You can only submit your own articles.');
  }
  if (!storedArticle.description || !isValid(storedArticle.description, scholarlyArticleDescriptionValidators)) {
    throw new Error('The article has to have a valid abstract before it can be submitted.');
  }
  if (!storedArticle.name || !isValid(storedArticle.name, scholarlyArticleNameValidators)) {
    throw new Error('The article has to have a valid name before it can be submitted.');
  }

  try {
    const result = await service(context.database, articleIdentifier, periodicalIdentifier, session.account);

    return {
      result: {
        author: { identifier: session.account.identifier },
        datePublished: result.datePublished,
        identifier: result.identifier,
        isPartOf: result.isPartOf,
      },
      targetCollection: {
        identifier: articleIdentifier,
      },
    };
  } catch (e) {
    // tslint:disable-next-line:no-console
    console.log('Database error:', e);

    throw new Error('There was a problem submitting the article, please try again.');
  }
}
