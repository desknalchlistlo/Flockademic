import {
  GetDashboardResponse,
  GetIsPeriodicalSlugAvailableResponse,
  GetPeriodicalContentsResponse,
  GetPeriodicalResponse,
  GetPeriodicalsResponse,
  GetPublishedScholarlyArticlesResponse,
  GetScholarlyArticleResponse,
  GetScholarlyArticlesResponse,
  PostCreatePeriodicalRequest,
  PostCreatePeriodicalResponse,
  PostGenerateUploadUrlForPeriodicalAssetsRequest,
  PostGenerateUploadUrlForPeriodicalAssetsResponse,
  PostGenerateUploadUrlForScholarlyArticleRequest,
  PostGenerateUploadUrlForScholarlyArticleResponse,
  PostInitialiseScholarlyArticleRequest,
  PostInitialiseScholarlyArticleResponse,
  PostMakePeriodicalPublicRequest,
  PostMakePeriodicalPublicResponse,
  PostPublishScholarlyArticleRequest,
  PostPublishScholarlyArticleResponse,
  PostSubmitScholarlyArticleRequest,
  PostSubmitScholarlyArticleResponse,
  PostUpdatePeriodicalRequest,
  PostUpdatePeriodicalResponse,
  PostUpdateScholarlyArticleRequest,
  PostUpdateScholarlyArticleResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { InitialisedPeriodical, Periodical } from '../../../../lib/interfaces/Periodical';
import { PublishedScholarlyArticle, ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { periodicalSlugValidators } from '../../../../lib/validation/periodical';
import { isValid } from '../../../../lib/validation/validators';
import { getSession, getToken } from './account';
import { fetchResource } from './fetchResource';

export async function initialiseJournal(): Promise<PostCreatePeriodicalResponse> {
  const request: PostCreatePeriodicalRequest = {};
  const headers = await getAuthHeaders();

  const response = await fetchResource(
    '/periodicals',
    { method: 'POST', body: JSON.stringify(request), headers },
  );

  const responseBody: PostCreatePeriodicalResponse = await response.json();

  return responseBody;
}

export async function initialiseArticle(
  periodicalSlug?: string,
  initialProperties?: Partial<ScholarlyArticle>,
)
: Promise<PostInitialiseScholarlyArticleResponse> {
  const request: PostInitialiseScholarlyArticleRequest = {
    result: {
      isPartOf: (periodicalSlug) ? { identifier: periodicalSlug } : undefined,
      ...initialProperties,
    },
   };
  const headers = await getAuthHeaders();

  const response = await fetchResource(
    '/periodicals/articles',
    { method: 'POST', body: JSON.stringify(request), headers },
  );

  const article: PostInitialiseScholarlyArticleResponse = await response.json();

  return article;
}

export async function getPeriodical(slug: string): Promise<Partial<Periodical>> {
  let token: string | undefined;

  const headers = new Headers();
  try {
    const session = await getSession();
    token = await getToken(session.identifier);
    headers.append('Authorization', `Bearer ${token}`);
  } catch (e) {
    // No need to do anything; we can just only fetch public Periodical details
  }

  const response = await fetchResource(`/periodicals/${slug}`, { headers });

  const periodical: GetPeriodicalResponse = await response.json();

  return periodical;
}

export async function getPeriodicals(): Promise<InitialisedPeriodical[]> {
  const response = await fetchResource(`/periodicals`);

  const periodicals: GetPeriodicalsResponse = await response.json();

  return periodicals;
}

export async function isPeriodicalSlugAvailable(slug: string): Promise<boolean> {
  const response = await fetchResource(`/periodicals/${slug}/exists`);

  const result: GetIsPeriodicalSlugAvailableResponse = await response.json();

  return (typeof result.error !== 'undefined');
}

export async function getPeriodicalContents(slug: string): Promise<GetPeriodicalContentsResponse> {
  const response = await fetchResource(`/periodicals/${slug}/articles`);

  const periodical: GetPeriodicalContentsResponse = await response.json();

  return periodical;
}

export async function getHeaderUploadUrl(
  periodicalId: string,
  filename: string,
): Promise<PostGenerateUploadUrlForPeriodicalAssetsResponse> {
  const headers = await getAuthHeaders();
  const request: PostGenerateUploadUrlForPeriodicalAssetsRequest = {
    result: {
      image: filename,
    },
    targetCollection: { identifier: periodicalId },
  };

  const response = await fetchResource(
    `/periodicals/${periodicalId}/upload/header`,
    { body: JSON.stringify(request), method: 'POST', headers },
  );

  const mediaObject: PostGenerateUploadUrlForPeriodicalAssetsResponse = await response.json();

  return mediaObject;
}

export async function getDashboard(): Promise<GetDashboardResponse> {
  const headers = await getAuthHeaders();

  const response = await fetchResource('/periodicals/dashboard', { headers });

  const dashboardContents: GetDashboardResponse = await response.json();

  return dashboardContents;
}

export async function getScholarlyArticle(identifier: string): Promise<Partial<ScholarlyArticle>> {
  let headers;
  try {
    headers = await getAuthHeaders();
  } catch (e) {
    // No need to do anything; we can just only fetch public article details
  }

  const response = await fetchResource(`/periodicals/articles/${identifier}`, { headers });

  const article: GetScholarlyArticleResponse = await response.json();

  return article;
}

export async function getRecentScholarlyArticles()
: Promise<PublishedScholarlyArticle[]> {
  const response = await fetchResource(
    '/periodicals/articles',
  );

  const articles: GetPublishedScholarlyArticlesResponse = await response.json();

  return articles;
}

export async function getScholarlyArticlesForAccount(accountId: string)
: Promise<Array<Partial<ScholarlyArticle>>> {
  const response = await fetchResource(
    `/periodicals/articles?author=${encodeURIComponent(accountId)}`,
  );

  const articles: GetScholarlyArticlesResponse = await response.json();

  return articles;
}

interface NewArticleValues {
  description: string;
  name: string;
}
export async function updateScholarlyArticle(
  identifier: string,
  newValues: NewArticleValues,
): Promise<PostUpdateScholarlyArticleResponse> {
  const headers = await getAuthHeaders();

  const request: PostUpdateScholarlyArticleRequest = {
    object: newValues,
    targetCollection: {
      identifier,
    },
  };

  const response = await fetchResource(
    `/periodicals/articles/${identifier}`,
    { body: JSON.stringify(request), method: 'PUT', headers },
  );

  const result: PostUpdateScholarlyArticleResponse = await response.json();

  return result;
}

export async function getArticleUploadUrl(
  articleId: string,
  filename: string,
  agreeWithLicense: true,
): Promise<PostGenerateUploadUrlForScholarlyArticleResponse> {
  const headers = await getAuthHeaders();
  const request: PostGenerateUploadUrlForScholarlyArticleRequest = {
    result: {
      associatedMedia: {
        license: (agreeWithLicense) ? 'https://creativecommons.org/licenses/by/4.0/' : null as any,
        name: filename,
      },
    },
    targetCollection: { identifier: articleId },
  };

  const response = await fetchResource(
    `/periodicals/articles/${articleId}/upload`,
    { body: JSON.stringify(request), method: 'POST', headers },
  );

  const mediaObject: PostGenerateUploadUrlForScholarlyArticleResponse = await response.json();

  return mediaObject;
}

export async function publishScholarlyArticle(
  articleId: string,
): Promise<PostPublishScholarlyArticleResponse> {
  const request: PostPublishScholarlyArticleRequest = {
    targetCollection: { identifier: articleId },
  };
  const headers = await getAuthHeaders();

  const response = await fetchResource(
    `/periodicals/articles/${articleId}/publish`,
    { body: JSON.stringify(request), method: 'POST', headers },
  );

  const data: PostPublishScholarlyArticleResponse = await response.json();

  return data;
}
export async function submitScholarlyArticle(
  articleId: string,
  periodicalId: string,
): Promise<PostSubmitScholarlyArticleResponse> {
  const request: PostSubmitScholarlyArticleRequest = {
    result: {
      isPartOf: {
        identifier: periodicalId,
      },
    },
    targetCollection: { identifier: articleId },
  };
  const headers = await getAuthHeaders();

  const response = await fetchResource(
    `/periodicals/${periodicalId}/submit/${articleId}`,
    { body: JSON.stringify(request), method: 'POST', headers },
  );

  const data: PostSubmitScholarlyArticleResponse = await response.json();

  return data;
}

export async function makePeriodicalPublic(
  periodicalId: string,
  slug: string,
): Promise<PostMakePeriodicalPublicResponse> {
  if (!isValid(slug, periodicalSlugValidators)) {
    throw new Error('This is not a valid Journal link');
  }

  const request: PostMakePeriodicalPublicRequest = {
    object: { identifier: slug },
    targetCollection: { identifier: periodicalId },
  };
  const headers = await getAuthHeaders();

  const response = await fetchResource(
    `/periodicals/${periodicalId}/publish`,
    { body: JSON.stringify(request), method: 'POST', headers },
  );

  const data: PostMakePeriodicalPublicResponse = await response.json();

  return data;
}

interface NewValues {
  description: string;
  headline: string;
  name: string;
}
export async function updatePeriodical(
  identifier: string,
  newValues: NewValues,
): Promise<PostUpdatePeriodicalResponse> {
  const headers = await getAuthHeaders();

  const request: PostUpdatePeriodicalRequest = {
    object: newValues,
    targetCollection: {
      identifier,
    },
  };

  const response = await fetchResource(
    `/periodicals/${identifier}`,
    { body: JSON.stringify(request), method: 'PUT', headers },
  );

  const result: PostUpdatePeriodicalResponse = await response.json();

  return result;
}

async function getAuthHeaders() {
  const session = await getSession();
  const token = await getToken(session.identifier);
  const headers = new Headers({
    Authorization: `Bearer ${token}`,
  });

  return headers;
}
