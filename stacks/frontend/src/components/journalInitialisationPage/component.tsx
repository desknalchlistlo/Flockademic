import './styles.scss';

import * as React from 'react';
import { Redirect, RouteComponentProps } from 'react-router';

import { initialiseJournal } from '../../services/periodical';
import { Spinner } from '../spinner/component';

export interface JournalInitialisationPageState {
  slug: string | undefined | null;
}
export interface JournalInitialisationPageRouteParams {}

export class JournalInitialisationPage extends React.Component<
  RouteComponentProps<JournalInitialisationPageRouteParams> | undefined,
  JournalInitialisationPageState
> {
  public state: JournalInitialisationPageState = {
    slug: undefined,
  };

  constructor(props?: RouteComponentProps<JournalInitialisationPageRouteParams>) {
    super(props);

    initialiseJournal()
    .then((response) => {
      this.setState({ slug: response.result.identifier });
    })
    .catch(() => {
      this.setState({ slug: null });
    });
  }

  public render() {
    return (
      <div className="section">
        <section className="section__primary-content">
          <div className="article">
            {this.getView()}
          </div>
        </section>
      </div>
    );
  }

  private getView() {
    if (typeof this.state.slug === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.slug === null) {
      return (
        <div className="callout alert">
          There was a problem starting your new Journal, please refresh this page to try again.
        </div>
      );
    }

    return <Redirect to={`/journal/${this.state.slug}/manage`}/>;
  }
}
